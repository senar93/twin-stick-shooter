﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovementScript : MyMonoBehaviour {

	/// <summary>
	/// velocità del player; 
	/// minimo "speedMin", massimo "speedMax"
	/// </summary>
	public float speed {
		get {
			return _speed;
		}
		set {
			_speed = Mathf.Clamp(value, speedMin, speedMax);
		}
	}

	/// <summary>
	/// velocità del player
	/// </summary>
	[SerializeField] private float _speed = 5;
	/// <summary>
	/// velocità minima
	/// </summary>
	[SerializeField] private float speedMin = 0.5f;
	/// <summary>
	/// velocità massima
	/// </summary>
	[SerializeField] private float speedMax = 10;
	
	/// <summary>
	/// puntatore al rigidbody del oggetto
	/// </summary>
	protected Rigidbody thisRigidbody;
	/// <summary>
	/// componente della velocità del oggetto (input del player sul asse X e Z)
	/// </summary>
	protected Vector3 inputVelocity = new Vector3(0, 0, 0);
	/// <summary>
	/// componente della velocità del oggetto (tutto cio che non è input del playersul asse X e Z)
	/// </summary>
	protected Vector3 otherVelocity = new Vector3(0, 0, 0);



	/// <summary>
	/// imposta il inputVelocity per il calcolo del movimento
	/// </summary>
	/// <param name="input"></param>
	public void PlanarMoveInput(Vector3 input) {
		inputVelocity.x = input.x;
		inputVelocity.z = input.z;

		inputVelocity = inputVelocity.normalized;
	}

	
	public void PlanarMoveOther(Vector3 newDirection, float newDirectionSpeed) {
		Vector3 tmpOtherVelocity = new Vector3(0, 0, 0);
		tmpOtherVelocity.x = newDirection.x;
		tmpOtherVelocity.z = newDirection.z;

		tmpOtherVelocity = tmpOtherVelocity.normalized;
		otherVelocity += tmpOtherVelocity * newDirectionSpeed;
	}

	/// <summary>
	/// calcola le velocità del oggetto escludendo gli input
	/// </summary>
	protected void CalcualteOtherVelocity() {
		otherVelocity = thisRigidbody.velocity - inputVelocity;
	}



	public void Update() {
		//CalcualteOtherVelocity();
		thisRigidbody.velocity = inputVelocity * speed + otherVelocity;
	}

	void Start() {
		thisRigidbody = GetSafeComponent<Rigidbody>();
	}

}
