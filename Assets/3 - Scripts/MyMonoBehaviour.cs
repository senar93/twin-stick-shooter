﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Custom MonoBehaviour
/// </summary>
[System.Serializable]
public class MyMonoBehaviour : MonoBehaviour {

	
	public T GetSafeComponent<T>() where T : Component {
		T component = this.GetComponent<T>();
		
		if(component == null) {
			Debug.LogError(this + " - Component of type \"" + typeof(T) + "\" was not found");
		}

		return component;
	}




}
