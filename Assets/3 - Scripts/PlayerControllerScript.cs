﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMovementScript))]
public class PlayerControllerScript : MyMonoBehaviour {

	/// <summary>
	/// nome input asse X
	/// </summary>
	public string inputX = "Horizontal";
	/// <summary>
	/// nome input asse Z
	/// </summary>
	public string inputZ = "Vertical";
	/// <summary>
	/// nome del input Shoot
	/// </summary>
	public string inputShoot = "Jump";
	public string[] inputOther;

	/// <summary>
	/// puntatore allo script che gestisce il movimento del player
	/// </summary>
	protected PlayerMovementScript movementScript;
	/// <summary>
	/// input inseriti dal giocatore
	/// </summary>
	protected Vector3 inputMovement = new Vector3(0,0,0);


	/// <summary>
	/// comunica a PlayerMovementScript gli input su X e Z
	/// </summary>
	private void PlanarMovement() {
		if (Input.GetAxis(inputX) != 0) {
			inputMovement.x = Input.GetAxis(inputX);
		} else {
			inputMovement.x = 0;
		}
		if (Input.GetAxis(inputZ) != 0) {
			inputMovement.z = Input.GetAxis(inputZ);
		} else {
			inputMovement.z = 0;
		}
		movementScript.PlanarMoveInput(inputMovement);
	}



	public void Start() {
		movementScript = GetComponent<PlayerMovementScript>();
	}

	public void FixedUpdate() {
		PlanarMovement();
	}

	

}
